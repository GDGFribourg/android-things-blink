package ch.gdgfribourg.blink

import android.app.Activity
import android.os.Bundle
import android.os.Handler
import android.util.Log
import com.google.android.things.pio.Gpio
import java.io.IOException

import com.google.android.things.pio.PeripheralManagerService

class HomeActivity : Activity() {

    val mHandler = Handler()
    var mLedGpio: Gpio? = null
    var mLedState = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        try {
            val service = PeripheralManagerService()
            mLedGpio = service.openGpio(BoardDefaults.gpioForLED)
        } catch (e: IOException) {
            Log.e(TAG, "Error on PeripheralIO API", e)
        }
        mLedGpio?.setDirection(Gpio.DIRECTION_OUT_INITIALLY_LOW)
        mHandler.post(mBlinkRunnable)
    }

    override fun onDestroy() {
        super.onDestroy()
        mHandler.removeCallbacks(mBlinkRunnable)
        Log.i(TAG, "Closing LED GPIO pin")
        try {
            mLedGpio?.close()
        } catch (e: IOException) {
            Log.e(TAG, "Error on PeripheralIO API", e)
        } finally {
            mLedGpio = null
        }
    }

    private val mBlinkRunnable: Runnable = object : Runnable {
        override fun run() {
            if (mLedGpio == null) return
            try {
                mLedState = !mLedState
                mLedGpio?.value = mLedState
                mHandler.postDelayed(this, INTERVAL_BETWEEN_BLINKS_MS)
            } catch (e: IOException) {
                Log.e(TAG, "Error on PeripheralIO API $e")
            }
        }
    }

    companion object {
        private val TAG = "HomeActivity"
        private val INTERVAL_BETWEEN_BLINKS_MS = 500L
    }
}
