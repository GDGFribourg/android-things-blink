package ch.gdgfribourg.blink

import android.os.Build
import com.google.android.things.pio.PeripheralManagerService

object BoardDefaults {
    private val DEVICE_EDISON_ARDUINO = "edison_arduino"
    private val DEVICE_EDISON = "edison"
    private val DEVICE_JOULE = "joule"
    private val DEVICE_RPI3 = "rpi3"
    private val DEVICE_PICO = "imx6ul_pico"
    private val DEVICE_VVDN = "imx6ul_iopb"
    private var sBoardVariant = ""

    /**
     * Return the GPIO pin that the LED is connected on.
     * For example, on Intel Edison Arduino breakout, pin "IO13" is connected to an onboard LED
     * that turns on when the GPIO pin is HIGH, and off when low.
     */
    val gpioForLED: String
        get() {
            when (boardVariant) {
                DEVICE_EDISON_ARDUINO -> return "IO13"
                DEVICE_EDISON -> return "GP45"
                DEVICE_JOULE -> return "LED100"
                DEVICE_RPI3 -> return "BCM6"
                DEVICE_PICO -> return "GPIO4_IO20"
                DEVICE_VVDN -> return "GPIO3_IO06"
                else -> throw IllegalStateException("Unknown Build.DEVICE ${Build.DEVICE}")
            }
        }

    private // For the edison check the pin prefix
            // to always return Edison Breakout pin name when applicable.
    val boardVariant: String
        get() {
            if (!sBoardVariant.isEmpty()) {
                return sBoardVariant
            }
            sBoardVariant = Build.DEVICE
            if (sBoardVariant == DEVICE_EDISON) {
                val pioService = PeripheralManagerService()
                val gpioList = pioService.gpioList
                if (gpioList.size != 0) {
                    val pin = gpioList[0]
                    if (pin.startsWith("IO")) {
                        sBoardVariant = DEVICE_EDISON_ARDUINO
                    }
                }
            }
            return sBoardVariant
        }
}